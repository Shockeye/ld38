﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSurfaceObject : MonoBehaviour
{
    //General stuff for keeping objects in position on world
    public SmallWorld world;

    Rigidbody rigidBody;
  
    // Use this for initialization
    void Start ()
    {
        GameObject go = GameObject.FindWithTag("SmallWorld");

        if (go)
        {
            world = go.GetComponent<SmallWorld>();
        }
        else
        {
            Debug.LogError("SmallWorld missing");
        }
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        rigidBody.useGravity = false;
    }

    void FixedUpdate() 
    {
        world.Gravity(transform);
    }


}
