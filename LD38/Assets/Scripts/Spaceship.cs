﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spaceship : MonoBehaviour
{
    //UI stuff
    public GameObject popupDisplay;
    public Text currentFuel;
    public RectTransform messagePanel;

    //Game stuff
    int crystals = 0;
    int requiredCrystals = 12;
    bool playerLoadingFuel = false;

    public CharacterController player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            Application.Quit();
        }
        if (playerLoadingFuel)
        {
            crystals += player.GetCrystal();
            currentFuel.text = crystals.ToString();
            if(crystals == requiredCrystals)
            {
                popupDisplay.SetActive(false);
                playerLoadingFuel = false;
                messagePanel.gameObject.SetActive(true);
                messagePanel.GetComponentInChildren<Text>().text = "You have collected enough fuel to launch./nYou have won!";
                player.gameObject.SetActive(false);
                crystals = 0;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //open spaceship UI 
        //drop off fuel
        //reload oxygen tanks
        if (other.CompareTag("Player"))
        {
            popupDisplay.SetActive(true);
            playerLoadingFuel = true;
            Debug.Log("triggered spaceship");
            currentFuel.text = crystals.ToString();
        }
    }

    void OnTriggerStay(Collider other)
    {

    }

    void OnTriggerExit(Collider other)
    {
        //close spaceship UI
        if (other.CompareTag("Player"))
        {
            popupDisplay.SetActive(false);
            playerLoadingFuel = false;
            Debug.Log("spaceship out of range");
        }
    }
}
