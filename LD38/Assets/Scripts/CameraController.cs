﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target; //object being tracked by cam
    public Vector3 offsetFromTarget = new Vector3(0.0f,0.0f,-10.0f); //position relative to target
    public float angleX = 20.0f; //camera angle / tilt (behind and above)
    float speed = 0.0f;

    Vector3 destination;
    

    void Update()
    {
        MoveToTarget();
        LookAtTarget();
    }

    void MoveToTarget()
    {
        destination = target.rotation * offsetFromTarget;
        destination += target.position;
        transform.position = destination;
    }

    void LookAtTarget()
    {

       //TODO: camera needs to stay tracking. not flipping when crossing poles etc (ambiguous resolve of angles)
        
        float angleY = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y,ref speed, 5.0f);
        //float angleX = Mathf.SmoothDampAngle(transform.eulerAngles.x, target.eulerAngles.x, ref speed, 5.0f);
        //float angleZ = Mathf.SmoothDampAngle(transform.eulerAngles.z, target.eulerAngles.z, ref speed, 5.0f);
        transform.rotation = Quaternion.Euler(0, angleY, 0);
        
       // transform.LookAt(target);

    }

    void Orbit()
    {

    }
}
