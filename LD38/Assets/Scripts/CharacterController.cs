﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    //UI stuff
    public Text oxygenReadout;
    public Text fuelReadout;
    public RectTransform messagePanel; 

    //game stuff
    int maxCrystals = 5; //carrying capacity
    int crystals = 0; //crystals carried by player
    float oxygen = 100.0f; //percent
    bool inSpaceship ; //TODO : ******* what happens when we spawn near space ship  *******
    float oxygenTimer = 0;
    //control stuff
    Rigidbody rigidBody;
    Vector3 direction;
    Vector3 movement;
    Vector3 currentVelocity;
    Animator animator;

    Quaternion targetRotation;

    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    
	// Use this for initialization
	void Start ()
    {
        direction = transform.forward;
        targetRotation = transform.rotation;
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
        if (inSpaceship )
        {
            oxygen = 100; //waiting o fill tanks is boring
            /*
            oxygen += 0.05f;
            if (oxygen > 100)
            {
                oxygen = 100.0f;
            }*/
        }
        else
        {
            oxygenTimer += Time.deltaTime;
            if (oxygenTimer > 1.0f)
            {
                oxygen -= 1f;
                oxygenTimer = 0;
            }
            if(oxygen < 0)
            {
                //???do we kill him here or on next loop
                //player has died
                oxygen = 0.0f;
                
                messagePanel.gameObject.SetActive(true);
                messagePanel.GetComponentInChildren<Text>().text = "player has asphyxiated./nGame Over.";
            }

        }
        StringBuilder value = new StringBuilder();
        value.Append(oxygen);
        value.Append("%");

        oxygenReadout.text= value.ToString();

    }

    void FixedUpdate()
    {
         rigidBody.MovePosition(rigidBody.position + transform.TransformDirection(movement) * Time.fixedDeltaTime);
    }

    void GetInput()
    {
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Horizontal") * Time.deltaTime * rotationSpeed);

        direction = new Vector3(0, 0, Input.GetAxisRaw("Vertical")).normalized;
        if (Input.GetAxisRaw("Vertical") > 0) animator.SetBool("Walk", true);
        else animator.SetBool("Walk", false);

        Vector3 targetMovement = direction * speed;
        
        movement = Vector3.SmoothDamp(movement, targetMovement,ref currentVelocity, 0.05f);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Crystal"))
        {
            crystals++;
            Destroy(other.gameObject);

            fuelReadout.text = crystals.ToString();
        }
        else if(other.CompareTag("Spaceship"))
        {
            inSpaceship = true;
            Debug.Log("in spaceship = true");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Spaceship"))
        {
            inSpaceship = false;
            Debug.Log("in spaceship = false");
        }
    }

    public int GetCrystal()
    {
        if (crystals > 0)
        {
            crystals--;
            fuelReadout.text = crystals.ToString();
            return 1;
        }
        else return 0;
    }

    public void StartGame()
    {
        crystals = 0; //crystals carried by player
        oxygen = 100.0f; //percent
        gameObject.SetActive(true);
        messagePanel.gameObject.SetActive(false);

    }
}
