﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallWorld : MonoBehaviour {
    float gravity = -100.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Gravity(Transform target)
    {
        Vector3 surfaceNormal = (target.position - transform.position).normalized;
        Vector3 targetUp = target.up;



        //Quaternion targetRotation = Quaternion.FromToRotation(targetUp, surfaceNormal) * target.rotation;
        //target.rotation = Quaternion.Slerp(target.rotation, targetRotation, 50 * Time.deltaTime);

        target.rotation =Quaternion.FromToRotation(targetUp, surfaceNormal) * target.rotation;
        

        target.GetComponent<Rigidbody>().AddForce(surfaceNormal * gravity);
    }
}
